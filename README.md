Para hacerlo funcionar, se necesita un contenedor de docker de RabbitMQ corriendo, y colocar los puertos correpondientes, o en otro caso, colocar la uri si se va a utilizar AmazonMQ.

Luego de eso, una vez con el RabbitMQ (o el servicio de mensajeria en general) encendido, desde dos terminales diferentes se ejecuta:
    -node persona1.js -> Para simular a una persona
    -node persona2.js -> Para simular a otra

Luego de eso simplemente seguir lo que pide, como el nombre de ambas personas (uno por terminal) y por consiguiente, proceder a mandar mensajes entre ambas terminales (las dos personas).

Nota: Funciona, pero hay un detalle, el primer mensaje siempre se recibe a si mismo, y ocurrre esto mismo al momento de querer mandar un segundo mensaje que provenga de la misma persona:
-     Ejemplo:
-     Dante envia: Hola
-     Persona2 recibe Hola
-     Dante envia sin que Persona2 halla enviado algo: Como estas. 
-     Dante recibe ese mismo mensaje.

