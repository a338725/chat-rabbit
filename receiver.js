const amqp = require('amqplib/callback_api');

const uri = "amqp://dante:dante@localhost:32773"

amqp.connect(uri, (err, con) => {
    if(err){
        throw err;
    }

    con.createChannel((err1, channel) => {
        if(err1){
            throw err1;
        }

        let queue = "mensajesDante";

        channel.assertQueue(queue, {
            durable: false
        });

        console.log("Esperando mensajes del servidor");

        channel.consume(queue, (message) => {
            console.log("Mensaje recibido: " + message.content.toString());
        }, {
            noAck: true
        });
    });
});