const amqp = require('amqplib/callback_api');
const readline = require('readline');

const uri = "amqp://dante:dante@localhost:32773"

amqp.connect(uri, (err, con) => {
    if(err){
        throw err;
    }

    con.createChannel((err1, channel) => {
        if(err1){
            throw err1;
        }

        const queue = "DanteIvan";

        channel.assertQueue(queue, {
            durable: false
        });

        console.log("Escriba sus mensajes");
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question('Nombre: ', (input2) => {
            console.log("Escriba sus mensajes");
            rl.on('line', (input) => {
                channel.sendToQueue(queue, Buffer.from(input));
                console.log(`${input2}: ` + input);
            });
        });

        channel.consume(queue, (message) => {
            console.log("Recibido: " + message.content.toString());
        }, {
            noAck: true
        });
    });
});
