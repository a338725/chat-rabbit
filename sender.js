const amqp = require('amqplib/callback_api');

// amqp://<user>?:<password>?@?<host>:<port>(5672)

const uri = "amqp://dante:dante@localhost:32773"

amqp.connect(uri, (err, con) => {
    if(err){
        throw err;
    }

    con.createChannel((err1, channel) => {
        if(err1){
            throw err1;
        }

        let message = "Hola mundo en RabbitMQ";
        let queue = "mensajesDante";

        channel.assertQueue(queue, {
            durable: false
        });

        channel.sendToQueue(queue, Buffer.from(message));
        console.log("Mensaje enviado");
    });

    setTimeout(() => {
        con.close();
        process.exit(0);
    }, 500);
});